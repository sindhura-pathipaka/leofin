<?php class site_model extends CI_Model

{

        // echo $this->db->last_query(); exit;
    public function get_cms($table, $id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get($table, $perpage = null)
    {
        $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
           }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $query = $this->db->get($table);
            // echo $this->db->last_query(); die;
        return $query->result();
    }

            public function get_by_where_column_2($table, $column1,$column1_value)
    {


        $this->db->where($column1, $column1_value);
        // $this->db->where($column2, $column2_value);

        $query = $this->db->get($table);
     // echo $this->db->last_query(); die; 

        return $query->row();

    }
    

    function get_result($table,$skill_id,$skill_name)
  {
      
    $this->db->where($skill_name,$skill_id);
    $query=$this->db->get($table);
    
     // echo $this->db->last_query(); die; 

    return $query->result();

  }




   function delete($table,$phone)
  {
  $result=$this->db->delete($table, array('phone' => $phone));
      // echo $this->db->last_query();  die();
     return true;
  }



    function update($table,$form_data,$col,$value)
  { 
    $this->db->update($table, $form_data, array($col => $value));
      // echo $this->db->last_query();  die();
    return true;
  }

    function update_status($table,$form_data,$col,$value,$col2,$value2)
  { 
    $this->db->update($table, $form_data, array($col  => $value,$col2  => $value2));
      // echo $this->db->last_query();  die();
    return true;
  }


    public function get_order_by_colmn_limit_page($table,$col,$order,$perpage = null,$limit = '')

    {
        $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $this->db->order_by($col, $order);
    if($limit != ""){
        $this->db->limit($limit);
    }
        $query = $this->db->get($table);
        return $query->result();
}

    public function get_order_by_colmn_limit($table,$col,$order,$limit = '')
    {
        $this->db->order_by($col, $order);
    if($limit != ""){
        $this->db->limit($limit);
    }
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->result();
}

    public function get_row_by_id_order_limit_row($table,$colmn_id,$colmn_value,$order_by_id,$order_by_value,$limit)

    {
        $this->db->where($colmn_id, $colmn_value);
        $this->db->order_by($order_by_id, $order_by_value);
        if($limit != ""){
        $this->db->limit($limit);
    }
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;

        return $query->row();
    }

        public function get_row_by_id($table,$colmn_id,$colmn_value,$limit= '')

    {
        $this->db->where($colmn_id, $colmn_value);
        // $this->db->order_by($order_by_value);
              if($limit != ""){
        $this->db->limit($limit);
    }
        $query = $this->db->get($table);

        // echo $this->db->last_query(); die;

        return $query->row();
    }

            public function get_result_by_id($table,$colmn_id,$colmn_value,$limit='')

    {
        $this->db->where($colmn_id, $colmn_value);
        // $this->db->order_by($order_by_value);
              if($limit != ""){
        $this->db->limit($limit);
    }
        $query = $this->db->get($table);

        // echo $this->db->last_query(); die;
        
        return $query->result();
    }

    public function get_row_by_id_order_limit_result($table,$colmn_id,$colmn_value,$order_by_id,$order_by_value)
    {
        $this->db->where($colmn_id, $colmn_value);

         if($id != ""){
        
        $this->db->where('id !=', $id);
    }


        $this->db->order_by($order_by_id, $order_by_value);
         if($limit != ""){
        $this->db->limit($limit);
    }
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->result();
    }

    public function get_by_p_link($table, $p_link)
    {
        $this->db->where('p_link', $p_link);
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->row();

    }


    public function get_by_number($table,$col_name,$col_value)
    {
        $this->db->where($col_name, $col_value);
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->row();

    }

    public function get_by_number2($table,$col_name,$col_value,$col_name2,$col_value2)
    {
        $this->db->where($col_name, $col_value);
        $this->db->where($col_name2, $col_value2);
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->row();

    }


     public function get_by_number3($table,$col_name,$col_value,$col_name2,$col_value2,$col_name3,$col_value3)
    {
        $this->db->where($col_name, $col_value);
        $this->db->where($col_name2, $col_value2);
        $this->db->where($col_name3, $col_value3);
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->row();

    }


     public function get_by_number4($table,$col_name,$col_value,$col_name2,$col_value2,$col_name3,$col_value3,$col_name4,$col_value4)
    {
        $this->db->where($col_name, $col_value);
        $this->db->where($col_name2, $col_value2);
        $this->db->where($col_name3, $col_value3);
        $this->db->where($col_name4, $col_value4);
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->row();

    }


     public function get_by_value($table,$col_name,$col_value)
    {
      $this->db->order_by('id','desc');
        $this->db->where($col_name, $col_value);
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->result();

    }


     public function get_by_value2($table,$col_name,$col_value,$col_name2,$col_value2)
    {
      $this->db->order_by('id','desc');
        $this->db->where($col_name, $col_value);
        $this->db->or_where($col_name2, $col_value2);
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->result();

    }

      public function get_by_value2_and($table,$col_name,$col_value,$col_name2,$col_value2)
    {
      $this->db->order_by('id','desc');
        $this->db->where($col_name, $col_value);
        $this->db->where($col_name2, $col_value2);
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;
        return $query->result();

    }




    public function get_by_letter($table, $heading,$letter)
    {

        $this->db->like($heading, $letter, 'after'); 
        $query = $this->db->get($table);

        return $query->result();
        // echo $this->db->last_query(); die;

    }
    

        public function get_by_search($table, $column1,$search)
    {

        $this->db->like($column1, $search); 
       
        $query = $this->db->get($table);
        // echo $this->db->last_query(); die;

        return $query->result();

    }
    

}