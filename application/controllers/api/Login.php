<?php
require (APPPATH . '/libraries/REST_Controller.php');
class Login extends CI_Controller
{
    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }




     function register() {

      if (!$this->input->get_post('name')) {
            $arr = array('err_code' => "invalid", "error_type" => "name_required", "message" => "Name is required");
            echo json_encode($arr);
            die;
        }
        if (!$this->input->get_post('mobile')) {
            $arr = array('err_code' => "invalid", "error_type" => "mobile required", "message" => "Mobile is required");
            echo json_encode($arr);
            die;
        }
     
         if (!$this->input->get_post('password')) {
            $arr = array('err_code' => "invalid", "error_type" => "password required", "message" => "Age is required");
            echo json_encode($arr);
            
            die;
        }
  $mobile = $this->input->get_post('mobile', TRUE);
          $query = $this->site_model->get_row_by_id('user_accounts','mobile',$mobile);
   if(count($query) == 0){

                  $form_data['device_id'] = $this->input->get_post('device_id', TRUE);
                  $form_data['name'] = $this->input->get_post('name', TRUE);
                  $form_data['email'] = $this->input->get_post('email', TRUE);
                  $form_data['mobile'] = $this->input->get_post('mobile', TRUE);
                  $form_data['password'] = md5($this->input->get_post('password', TRUE));
                  $generated_token =  $this->generatereg_no();
                  $form_data['reg_no'] =$generated_token;
                   $form_data['account_status'] = 'Need to be Approved';
                 $otp = $this->generateOTP(); 
                  $form_data['otp'] = $otp;  

            date_default_timezone_set('Asia/Kolkata');

        $form_data['created_at'] =  date( 'd-m-Y h:i:s A');
              
          // send_message('Hi, Welcome to Swecha App, your OTP is '.$form_data['otp'],$form_data['mobile'], '1407161155581637054');

            $this->cmoon_model->insert('user_accounts',$form_data);
           //    $sess_array = array(
           //   'reg_no' => $form_data['reg_no'],
           //   'client_id' => $this->db->insert_id(),
           // );
           // $this->session->set_userdata('user_create',$sess_array);

             $user_id= $this->db->insert_id();


        $data['result'] = $this->db->get_where('user_accounts',['id'=>$user_id])->row(); 
            // echo $this->db->last_query(); die;

                    $arr = array(
                        'err_code' => "valid",
                        "device_id" => $data['result']->device_id,
                        "user_id" => $data['result']->id,
                        "reg_no" => $data['result']->reg_no,
                        "otp" => $data['result']->otp,
                        "message" => " successful",

                    );
            echo json_encode($arr);
        // } else {
        //     $arr = array('err_code' => "invalid", "message" => "Plaese Check and submit");
        //     echo json_encode($arr);
        // }
      }else{
           $arr = array('err_code' => "invalid", "message" => "Already registered with the given phone number.");
            echo json_encode($arr);
      }
}




  function otp_verification() {
        if (!$this->input->get_post('otp')) {
            $arr = array('err_code' => "invalid", "error_type" => "otp required", "message" => "OTP is required");
            echo json_encode($arr);
            die;
        }
 
        $device_id=$this->input->get_post('device_id');
        $user_id=$this->input->get_post('user_id');
       $reg_no=$this->input->get_post('reg_no');
       $otp = $this->input->get_post('otp', TRUE);
        $data['user_accounts'] = $this->site_model->get_by_number4('user_accounts','device_id',$device_id,'id',$user_id,'reg_no',$reg_no,'otp',$otp);


          if(count($data['user_accounts']) == 0){ 
            $arr = array('err_code' => "invalid", "message" => "Please enter correct OTP.");
            echo json_encode($arr);
          }else{
           $arr = array(
                        'err_code' => "valid",
                        "message" => "successful",
                    );
                    echo json_encode($arr);
      }
}


 function resend_otp() {
   
        $device_id=$this->input->get_post('device_id');
        $user_id=$this->input->get_post('user_id');
       $reg_no=$this->input->get_post('reg_no');
        $data['user_accounts'] = $this->site_model->get_by_number3('user_accounts','device_id',$device_id,'id',$user_id,'reg_no',$reg_no);
          if(count($data['user_accounts']) != 0){ 

             $otp = $this->generateOTP(); 
              $form_data['otp'] = $otp;  
              
              // send_message('Hi, Welcome to Swecha App, your OTP is '.$form_data['otp'], $data['user_accounts']->mobile, '1407161155581637054');
              
                  $result = $this->cmoon_model->update('user_accounts', $form_data,$user_id);
      // echo $this->db->last_query();  die();
        $data['otp'] = $this->db->get_where('user_accounts',['id'=>$user_id])->row(); 

            $arr = array('err_code' => "valid",
                        "user_id" => $data['otp']->id,
                        "reg_no" => $data['otp']->reg_no,
                        "otp" => $data['otp']->otp,
                        "title" => " successful",
                         "message" => "OTP has been sent to your Number");
            echo json_encode($arr);
          }else{
           $arr = array(
                        'err_code' => "invalid",
                        "title" => "Failed",
                    );
                    echo json_encode($arr);
      }
}


  function index() {

        if (!$this->input->get_post('mobile')) {
            $arr = array('status' => "invalid", "status" => "mobile required", "message" => "mobile is required");
            echo json_encode($arr);
            die;
        }

         if (!$this->input->get_post('password')) {
            $arr = array('status' => "invalid",  "message" => "password is required");
            echo json_encode($arr);
            die;
        }

       $mobile = $this->input->get_post('mobile', TRUE);
       $password = md5($this->input->get_post('password', TRUE));
       $device_id = $this->input->get_post('device_id', TRUE);

      $data['user_accounts'] = $this->site_model->get_by_number3('user_accounts','device_id',$device_id,'mobile',$mobile,'password',$password);


           if(count($data['user_accounts']) == 0){ 
            $arr = array('status' => "invalid", "message" => "Please enter correct details ");
            echo json_encode($arr);
          }elseif($data['user_accounts']->device_id != $device_id){ 
            $arr = array('status' => "invalid", "message" => "You have not registered in this device.");
            echo json_encode($arr);
          }elseif($data['user_accounts']->account_status == "Need to be Approved"){ 
            $arr = array('status' => "invalid", "message" => "You Account Need to be Approved by Admin.");
            echo json_encode($arr);
          }elseif($data['user_accounts']->account_status == "Rejected"){ 
            $arr = array('status' => "invalid", "message" => "You Account is Rejected by Admin.");
            echo json_encode($arr);
          }else{

            date_default_timezone_set('Asia/Kolkata');
        $user_log_data['login_time'] =  date('d-m-Y h:i:s A');
        $user_log_data['mobile'] =  $data['user_accounts']->mobile;
        $user_log_data['ip_address'] = $_SERVER['REMOTE_ADDR'];

        $result = $this->cmoon_model->insert('user_logs',$user_log_data);

  if($result){
                    $arr = array(
                        'status' => "valid",
                        "message" => "successful",
                        // "data" => $data['user_accounts'],
                    );
                    echo json_encode($arr);
        } else {
            $arr = array('status' => "invalid", "message" => "Plaese Try again");
            echo json_encode($arr);
        }
 
      }
}

  



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


private function generateOTP()  {
        $seed = str_split('0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 4) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }




private function generatereg_no()  {
        $seed = str_split('0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 6) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }




// -------------------------------------- Image upload code -----------------------------------------------------
    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "profile_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "*";
        $config1['max_size']      = "16000000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     =  $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();

        return $fileDetailArray1['file_name'];
    }
    }