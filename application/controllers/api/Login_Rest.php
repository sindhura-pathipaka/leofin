<?php
require (APPPATH . '/libraries/REST_Controller.php');
class Login extends REST_Controller
{

    function __construct(){
      parent:: __construct();
      $this->load->model('login_model','',True);
      $this->load->model('cmoon_model','',True);
      $this->load->model('site_model','',True);
   }




     function register_post() {

      if (!$this->input->get_post('name')) {
            $arr = array('err_code' => "invalid", "error_type" => "name_required", "message" => "Name is required");
            $this->response($arr);
            die;
        }
        if (!$this->input->get_post('mobile')) {
            $arr = array('err_code' => "invalid", "error_type" => "mobile required", "message" => "Mobile is required");
            $this->response($arr);
            die;
        }
     
         if (!$this->input->get_post('password')) {
            $arr = array('err_code' => "invalid", "error_type" => "password required", "message" => "Age is required");
            $this->response($arr);
            
            die;
        }
  $mobile = $this->input->get_post('mobile', TRUE);
          $query = $this->site_model->get_row_by_id('user_accounts','mobile',$mobile);
   if(count($query) == 0){

                  $form_data['name'] = $this->input->get_post('name', TRUE);
                  $form_data['email'] = $this->input->get_post('email', TRUE);
                  $form_data['mobile'] = $this->input->get_post('mobile', TRUE);
                  $form_data['password'] = md5($this->input->get_post('password', TRUE));
                  $generated_token =  $this->generatereg_no();
                  $form_data['reg_no'] =$generated_token;
                 
            date_default_timezone_set('Asia/Kolkata');

        $form_data['created_at'] =  date( 'd-m-Y h:i:s A');

            $this->cmoon_model->insert('user_accounts',$form_data);
           //    $sess_array = array(
           //   'reg_no' => $form_data['reg_no'],
           //   'client_id' => $this->db->insert_id(),
           // );
           // $this->session->set_userdata('user_create',$sess_array);

             $user_id= $this->db->insert_id();


        $data['result'] = $this->db->get_where('user_accounts',['id'=>$user_id])->row(); 
            // echo $this->db->last_query(); die;

                    $arr = array(
                        'err_code' => "valid",
                        "user_id" => $data['result']->id,
                        "reg_no" => $data['result']->reg_no,
                        "message" => " successful",

                    );
            $this->response($arr);
        // } else {
        //     $arr = array('err_code' => "invalid", "message" => "Plaese Check and submit");
        //     echo json_encode($arr);
        // }
      }else{
           $arr = array('err_code' => "invalid", "message" => "Already registered with the given phone number.");
            $this->response($arr);
      }
}



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


private function generateOTP()  {
        $seed = str_split('0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 4) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }




private function generatereg_no()  {
        $seed = str_split('0123456789'); // and any other characters
        shuffle($seed); // probably optional since array is randomized; this may be redundant
        $otp = '';
        foreach (array_rand($seed, 6) as $k)
        {
           $otp .= $seed[$k];
        }
        return $otp;
    }




// -------------------------------------- Image upload code -----------------------------------------------------
    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "profile_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "*";
        $config1['max_size']      = "16000000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     =  $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();

        return $fileDetailArray1['file_name'];
    }
    }