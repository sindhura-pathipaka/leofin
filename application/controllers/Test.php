<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    ob_start();
class Test extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('excel_import_model', 'export');
        $this->load->model('cmoon_model', '', true);
        $this->load->model('site_model', '', true);
        // $this->load->model('cmoon_model');
        $this->load->library('excel');
    }
    function index() {
        $this->load->view('cmoon/test');
    }
   



    function import_data() {
// echo 'hie';
// die;
        if (isset($_FILES["file"]["name"])) {
// echo 'hi';
// die;

            $this->db->truncate('districts');
            $this->db->truncate('subdistricts');
            $this->db->truncate('subsubdistricts');
            $this->db->truncate('data');

            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach ($object->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for ($row = 2; $row <= $highestRow; $row++) {

                    $u_disc_code = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $name = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $school_cat = $worksheet->getCellByColumnAndRow(2, $row)->getValue();



                    $school_type = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $sanctioned_strength = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $mandal = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $school_complex = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $assembly_consitency = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $parliment_consitency = $worksheet->getCellByColumnAndRow(8, $row)->getValue();

                     $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                     $password = substr(str_shuffle($chars), 0, 10);
                  
                    


                    if (isset($u_disc_code) && !empty($u_disc_code)) {
                    

                        $form_data = array(
                           
                            'u_disc_code' => $u_disc_code,
                            'name' => $name,
                            'school_cat' => $school_cat,
                            'school_type' => $school_type,
                            'sanctioned_strength' => $sanctioned_strength,
                            'mandal' => $mandal,
                            'school_complex' => $school_complex,
                            'assembly_consitency' => $assembly_consitency,
                            'parliment_consitency' => $parliment_consitency,
                            'password' => $password,

                            
                        );
                       
                        $insert = $this->cmoon_model->insert('school_table', $form_data);

                            // $school_id=$this->db->insert_id();

                            // echo $school_id;
                        // echo $this->db->last_query(); die;

                     

  
                    
                }
            }
            
            if ($insert) {
                redirect('cmoon/excel_view/Success');
                // echo "success";
            } else {
                redirect('cmoon/excel_add/error');
                // echo "failed";
            }
        }
    }

}


    private function permalink($string)
    {
        // $string=$string."-".rand(0,9999);
        $string = str_replace(" ", "-", $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

}

