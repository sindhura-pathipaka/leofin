<?php ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
class Cmoon_login extends CI_Controller {
      public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model','',True);
        $this->load->model('cmoon_model', '', true);
        $this->data["site_details"]=$this->cmoon_model->get_row_by_row('site_details', '1');
    }
  public function index()
  {
    if($this->input->post()){
            $form_data = $_SERVER['REMOTE_ADDR'];           
            $row_there = $this->login_model->get_ip('admin_ips',$form_data);
            if($row_there > 0){
                 $username=$this->input->post('username');
                 $password=sha1($this->input->post('password'));
         $row=$this->login_model->login($username,$password,'login');
            if($row != ""){
                $sess_arr=array(
                'id'=>$row->id,
                'username'=>$row->username,
                );
                $data=$this->session->set_userdata('logged_in',$sess_arr);
                redirect('cmoon/index');
        }else{
            $this->session->set_flashdata('login_error','Username or Password is not correct');
            redirect('cmoon_login');
        }
            }else{
                $this->session->set_flashdata('login_error','You dont have administrative access');
                redirect('cmoon_login');
            }
      }else{
        $this->load->view('cmoon/login',$this->data);
      }
  }
    function logout()
  {
      $this->session->unset_userdata('logged_in');
      $this->session->set_flashdata('logout_success','You Have logged out successfully');
      redirect('cmoon_login');
  }
    function dip(){  
        $form_data = $_SERVER['REMOTE_ADDR'];
        $result=$this->login_model->insert('admin_ips',$form_data);
            if($result==true){
                $this->session->set_flashdata('logout_success','Now you have access to admin panel');
                redirect('cmoon_login');
            }
         }
   public function forgot()
  {
    $this->load->view('cmoon/forgot',$this->data);
  }
    function reset_password(){
        $form_data=$this->input->post();
        $result = $this->login_model->get_email('site_details',$form_data['email']);
        if($result > 0){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr(str_shuffle($chars), 0, 10);
        $password1 = sha1($password);
        $result_psw = $this->login_model->update_password('login', '1', $password1);
        if($result_psw == true){
       $to_mail = $this->data["site_details"]->forgot_email;
       $from_email = $this->data["site_details"]->from_email;
       $site_name = $this->data["site_details"]->site_name;
       $email_message = "Hi Admin,<br>
      Your Password of the site $site_name has been reset as per your request  <br>
      Your new password is : $password <br>";
       //echo $message;exit;
    $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();
require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');
//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();
//-----------Enable SMTP debugging. ---------------
// $mail->SMTPDebug = 3;                               
// //Set PHPMailer to use SMTP.
// $mail->isSMTP();            
// //Set SMTP host name                          
// $mail->Host = "cmdemo.in";
// //Set this to true if SMTP host requires authentication to send email
// $mail->SMTPAuth = true;                          
// //Provide username and password     
// $mail->Username = "hyd@cmdemo.in";                 
// $mail->Password = "hyd@123";                           
// //If SMTP requires TLS encryption then set it
// $mail->SMTPSecure = "tls";                           
// //Set TCP port to connect to 
// $mail->Port = 465; 
// $client_email=$email;
//-------------------- END SMTP --------------------
//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;
//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional
//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");
//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");
//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = "Reset password of the site $site_name";
$mail->Body = $email_message;
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();
       // $this->load->library('email');
       // $this->email->from($from_email);
       // $this->email->to($to_mail);
       // $this->email->subject("Rest password of the site $site");
       // $this->email->message($email_message);
       //  $email_from = $from_email;
       //  $headers = "MIME-Version: 1.0" . "\r\n";
       //  $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
       //  $headers .= 'From: '.$email_from. "\r\n";
       //  $headers .= 'Reply-To: '.$email_from. "\r\n";
       //  $mail=mail($to_mail, $site ,$message, $headers);
       //$send = $this->email->send();
       if ($sucess) {
            $this->session->set_flashdata('logout_success','Your default password has been sent to the registered Mail-Id');
            redirect('cmoon_login');
       } else {
            $this->session->set_flashdata('login_error','Unable to reset password please try after refresing the page');
            redirect('cmoon_login/forgot');
       }
             }else{
               $this->session->set_flashdata('login_error','unable to reset password please try after some time');
                redirect('cmoon_login/forgot');
             }
        }else{
            $this->session->set_flashdata('login_error','Email-Id does not match with the registered Email-Id');
           redirect('cmoon_login/forgot');
       }
    }
}