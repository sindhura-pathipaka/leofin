<?php ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmoon extends CI_Controller {


	    public function __construct()
    {
        parent::__construct();
        // $this->load->model('login_model','',True);
        $this->load->model('cmoon_model', '', true);
        $this->data["site_details"]=$this->cmoon_model->get_row_by_row('site_details', '1');
    }


	public function index()
	{
		$this->load->view('cmoon/include/header',$this->data);
		$this->load->view('cmoon/index');
		$this->load->view('cmoon/include/footer');
	}


// ----------------------------------------------  Change password code    -------------------------------------------

    public function change_password()
    {
    	
        if ($this->input->post()) {

            // $form_data = $this->input->post();
            $password = sha1($this->input->post('password'));
            $row_there = $this->cmoon_model->get_psw('login', '1', $password);
            if ($row_there == 1) {
                $new_password = sha1($this->input->post('new_password'));
                $result = $this->cmoon_model->update_psw('login', '1', $new_password);
                if ($result == true) {
                    redirect('cmoon/change_password/psw_Success');
                } else {
                    redirect('cmoon/change_password/psw_error');
                }
            } else {
                redirect('cmoon/change_password/psw_doesnot_match');
            }
        } else {
            $this->load->view('cmoon/include/header',$this->data);
            $data['result'] = $this->cmoon_model->get('login', '1');
            $this->load->view('cmoon/change_password', $data);
            $this->load->view('cmoon/include/footer');
        }
    }


// -------------------------------------------------------------------------------------------------------




    // --------------------------------------    Home Banners code    ------------------------------------------

  public function home_banners_view()
  {
    $this->load->view('cmoon/include/header',$this->data);
    $data['result'] = $this->cmoon_model->get('home_banners');
    $this->load->view('cmoon/home_banners_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function home_banners_add($id = '')
  {
       
    if($_FILES['image']['name'] != ''){


      $form_data=$this->input->post();
        
      if($id != ''){
                $del_existing_img = $this->cmoon_model->get_row_by_row('home_banners', $id);
       if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/".$del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }                                   
        $result = $this->cmoon_model->update('home_banners',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/home_banners_view/Success');
            } else {
                redirect('cmoon/home_banners_add/error');
            }
      }else{


                $images = $_FILES['image']['name'];
               $this->load->library('upload');
               $this->load->library('image_lib');
               $upload_conf = array(
                   'upload_path'   => realpath('cmoon_images'),
                   'allowed_types' => '*',
                   'max_size'      => '30000',
                   // 'encrypt_name'  => true,
               );
               $this->upload->initialize($upload_conf);
               foreach ($_FILES['image'] as $key => $val) {
                   $i = 1;
                   foreach ($val as $v) {
                       $field_name                = "file_" . $i;
                       $_FILES[$field_name][$key] = $v;
                       $i++;
                   }
               }
               unset($_FILES['image']);
               $error   = array();
               $success = array();
               foreach ($_FILES as $field_name => $file) {
                   if (!$this->upload->do_upload($field_name)) {
                       $error['upload'][] = $this->upload->display_errors();
                   } else {
                       $upload_data        = $this->upload->data();
                       $success[]          = $upload_data;
                       $form_data['image'] = $upload_data['file_name'];
                       $result = $this->cmoon_model->insert('home_banners',$form_data);
                   }
               }
               if ($result == true) {
                   redirect('cmoon/home_banners_view/Success');
               } else {
                   redirect('cmoon/home_banners_add/error');
               }
        

  
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('home_banners',$id);
      }
      $this->load->view('cmoon/home_banners_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function home_banners_delete($id)
    {


        $del_existing_img = $this->cmoon_model->get_row_by_row('home_banners', $id);
            unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('home_banners', $id);
        if ($result == true) {
            redirect('cmoon/home_banners_view/dSuccess');
        } else {
            redirect('cmoon/home_banners_add/error');
        }
    }


// --------------------------------------     CMS Pages code    ------------------------------------------

	public function cms_pages_view()

	{
		$this->load->view('cmoon/include/header',$this->data);		
		$data['result'] = $this->cmoon_model->get('cms_pages');
		$this->load->view('cmoon/cms_pages_view',$data);
		$this->load->view('cmoon/include/footer');
	}

	public function cms_pages_edit($id = '')
	{
		if($this->input->post()){

			$form_data=$this->input->post();
            $form_data['p_link'] = $this->permalink($form_data['heading']);
            $redirect =$form_data['redirect'];


			if($id != ''){

    //             $del_existing_img = $this->cmoon_model->get_row_by_row('cms_pages', $id);
                
			 // if ($_FILES['image']['name'] != '') {
    //                 unlink("cmoon_images/".$del_existing_img->image);
    //                 $form_data['image'] = $this->upload_file('image');
    //             }


      
				$result = $this->cmoon_model->update('cms_pages',$form_data,$id);

			if ($result == true) {
                redirect('cmoon/'. $redirect .'/Success');
            } else {
                redirect('cmoon/cms_pages_edit/error');
            }

			}else{


              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //   }
           
				$result = $this->cmoon_model->insert('cms_pages',$form_data);

			if ($result == true) {
                redirect('cmoon/cms_pages_view/Success');
            } else {
                redirect('cmoon/cms_pages_edit/error');
            }
			}

		}else{

			$this->load->view('cmoon/include/header',$this->data);
			if($id != ''){
				$data['result'] = $this->cmoon_model->get_row_by_result('cms_pages',$id);
			}
			$this->load->view('cmoon/cms_pages_edit',$data);
			$this->load->view('cmoon/include/footer');
		}
	}

	   public function cms_pages_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('cms_pages', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('cms_pages', $id);
        if ($result == true) {
            redirect('cmoon/cms_pages_view/dSuccess');
        } else {
            redirect('cmoon/cms_pages_edit/error');
        }
    }

// --------------------------------------     properties code    ------------------------------------------

  public function properties_view()
  {
    $this->load->view('cmoon/include/header',$this->data);    
    $data['result'] = $this->cmoon_model->get('properties');
    $this->load->view('cmoon/properties_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function properties_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
                $del_existing_img = $this->cmoon_model->get_row_by_row('properties', $id);
       if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/".$del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
        $result = $this->cmoon_model->update('properties',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/properties_view/Success');
            } else {
                redirect('cmoon/properties_add/error');
            }
      }else{

date_default_timezone_set('Asia/Kolkata');

            $form_data['posted_on'] = date('d-M-Y');
            $form_data['str_time'] = strtotime((date('d-M-Y')));

              if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
               }else {
                     redirect('cmoon/properties_add/image_error');
                   }
        $result = $this->cmoon_model->insert('properties',$form_data);

      if ($result == true) {
                redirect('cmoon/properties_view/Success');
            } else {
                redirect('cmoon/properties_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('properties',$id);
      }
      $this->load->view('cmoon/properties_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function properties_delete($id)
    {


        $del_existing_img = $this->cmoon_model->get_row_by_row('properties', $id);
            unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('properties', $id);
        if ($result == true) {
            redirect('cmoon/properties_view/dSuccess');
        } else {
            redirect('cmoon/properties_add/error');
        }
    }

    // --------------------------------------     loan_categories code    ------------------------------------------

  public function loan_categories_view()
  {
    $this->load->view('cmoon/include/header',$this->data);    
    $data['result'] = $this->cmoon_model->get('loan_categories');
    $this->load->view('cmoon/loan_categories_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function loan_categories_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('loan_categories', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('loan_categories',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/loan_categories_view/Success');
            } else {
                redirect('cmoon/loan_categories_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/loan_categories_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('loan_categories',$form_data);

      if ($result == true) {
                redirect('cmoon/loan_categories_view/Success');
            } else {
                redirect('cmoon/loan_categories_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('loan_categories',$id);
      }
      $this->load->view('cmoon/loan_categories_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function loan_categories_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('loan_categories', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('loan_categories', $id);
        if ($result == true) {
            redirect('cmoon/loan_categories_view/dSuccess');
        } else {
            redirect('cmoon/loan_categories_add/error');
        }
    }

// --------------------------------------     real_estate_categories code    ------------------------------------------

  public function real_estate_categories_view()
  {
    $this->load->view('cmoon/include/header',$this->data);    
    $data['result'] = $this->cmoon_model->get('real_estate_categories');
    $this->load->view('cmoon/real_estate_categories_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function real_estate_categories_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('real_estate_categories', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('real_estate_categories',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/real_estate_categories_view/Success');
            } else {
                redirect('cmoon/real_estate_categories_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/real_estate_categories_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('real_estate_categories',$form_data);

      if ($result == true) {
                redirect('cmoon/real_estate_categories_view/Success');
            } else {
                redirect('cmoon/real_estate_categories_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('real_estate_categories',$id);
      }
      $this->load->view('cmoon/real_estate_categories_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function real_estate_categories_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('real_estate_categories', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('real_estate_categories', $id);
        if ($result == true) {
            redirect('cmoon/real_estate_categories_view/dSuccess');
        } else {
            redirect('cmoon/real_estate_categories_add/error');
        }
    }



// --------------------------------------     insurance_categories code    ------------------------------------------

  public function insurance_categories_view()
  {
    $this->load->view('cmoon/include/header',$this->data);    
    $data['result'] = $this->cmoon_model->get('insurance_categories');
    $this->load->view('cmoon/insurance_categories_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function insurance_categories_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('insurance_categories', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('insurance_categories',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/insurance_categories_view/Success');
            } else {
                redirect('cmoon/insurance_categories_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/insurance_categories_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('insurance_categories',$form_data);

      if ($result == true) {
                redirect('cmoon/insurance_categories_view/Success');
            } else {
                redirect('cmoon/insurance_categories_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('insurance_categories',$id);
      }
      $this->load->view('cmoon/insurance_categories_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function insurance_categories_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('insurance_categories', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('insurance_categories', $id);
        if ($result == true) {
            redirect('cmoon/insurance_categories_view/dSuccess');
        } else {
            redirect('cmoon/insurance_categories_add/error');
        }
    }


// --------------------------------------     faqs code    ------------------------------------------

  public function faqs_view()
  {
    $this->load->view('cmoon/include/header',$this->data);    
    $data['result'] = $this->cmoon_model->get('faqs');
    $this->load->view('cmoon/faqs_view',$data);
    $this->load->view('cmoon/include/footer');
  }

  public function faqs_add($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('faqs', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('faqs',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/faqs_view/Success');
            } else {
                redirect('cmoon/faqs_add/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/faqs_add/image_error');
              //      }
        $result = $this->cmoon_model->insert('faqs',$form_data);

      if ($result == true) {
                redirect('cmoon/faqs_view/Success');
            } else {
                redirect('cmoon/faqs_add/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('faqs',$id);
      }
      $this->load->view('cmoon/faqs_add',$data);
      $this->load->view('cmoon/include/footer');
    }
  }

     public function faqs_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('faqs', $id);
        //     unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('faqs', $id);
        if ($result == true) {
            redirect('cmoon/faqs_view/dSuccess');
        } else {
            redirect('cmoon/faqs_add/error');
        }
    }




// --------------------------------------    user_accounts code    ------------------------------------------

    public function user_accounts()
    {
        $this->load->view('cmoon/include/header',$this->data);
        $data['result'] = $this->cmoon_model->get('user_accounts');
        $this->load->view('cmoon/user_accounts',$data);
        $this->load->view('cmoon/include/footer');
    }


public function user_accounts_edit($id = '')
  {
    if($this->input->post()){

      $form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);


      if($id != ''){
       //          $del_existing_img = $this->cmoon_model->get_row_by_row('faqs', $id);
       // if ($_FILES['image']['name'] != '') {
       //              unlink("cmoon_images/".$del_existing_img->image);
       //              $form_data['image'] = $this->upload_file('image');
       //          }
        $result = $this->cmoon_model->update('user_accounts',$form_data,$id);

      if ($result == true) {
                redirect('cmoon/user_accounts/Success');
            } else {
                redirect('cmoon/user_accounts_edit/error');
            }
      }else{

              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //  }else {
              //        redirect('cmoon/user_accounts_edit/image_error');
              //      }
        $result = $this->cmoon_model->insert('user_accounts',$form_data);

      if ($result == true) {
                redirect('cmoon/user_accounts/Success');
            } else {
                redirect('cmoon/user_accounts_edit/error');
            }
      }

    }else{

      $this->load->view('cmoon/include/header',$this->data);
      if($id != ''){
        $data['result'] = $this->cmoon_model->get_row_by_result('user_accounts',$id);
      }
      $this->load->view('cmoon/user_accounts_edit',$data);
      $this->load->view('cmoon/include/footer');
    }
  }
       public function user_accounts_delete($id)
    {


        // $del_existing_img = $this->cmoon_model->get_row_by_row('user_accounts', $id);
            // unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('user_accounts', $id);
       
        if ($result == true) {
            redirect('cmoon/user_accounts/dSuccess');
        } else {
            redirect('cmoon/user_accounts/error');
        }
    }


// ---------------------------------------------------------------------------------------------------------

// --------------------------------------    Site Details code    ------------------------------------------


    public function site_details()
    {
        if ($this->input->post()) {
            $form_data = $this->input->post();
        $del_existing_img = $this->cmoon_model->get_row_by_row('site_details', '1');
            if ($_FILES['site_logo']['name'] != '') {
                unlink("cmoon_images/".$del_existing_img->site_logo);
                $form_data['site_logo'] = $this->upload_file('site_logo');
            }
            if ($_FILES['site_favicon']['name'] != '') {
                unlink("cmoon_images/".$del_existing_img->site_favicon);
                $form_data['site_favicon'] = $this->upload_file('site_favicon');
            }
            if ($_FILES['footer_logo']['name'] != '') {
                unlink("cmoon_images/".$del_existing_img->footer_logo);
                $form_data['footer_logo'] = $this->upload_file('footer_logo');
            }
            $result = $this->cmoon_model->update('site_details', $form_data,'1');
            if ($result == true) {
                redirect('cmoon/site_details/Success');
            } else {
                redirect('cmoon/site_details/error');
            }
        } else {
            $this->load->view('cmoon/include/header',$this->data);
            $data['result'] = $this->cmoon_model->get_row_by_row('site_details', '1');
            // $data['result'] = $this->db->get_where('site_details')->row();
            $this->load->view('cmoon/site_details', $data);
            $this->load->view('cmoon/include/footer');
        }
    }
// --------------------------------------     Social links code    ------------------------------------------

    public function social_links()
    {
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $result = $this->cmoon_model->update('social_links',$form_data, '1');
            if ($result == true) {
                redirect('cmoon/social_links/Success');
            } else {
                redirect('cmoon/social_links/error');
            }
        } else {
            $this->load->view('cmoon/include/header',$this->data);
            // $data['result'] = $this->db->get_where('social_links')->row();
            $data['result'] = $this->cmoon_model->get_row_by_row('social_links', '1');
            $this->load->view('cmoon/social_links', $data);
            $this->load->view('cmoon/include/footer');
        }
    }


// --------------------------------------     Seo Tags code    ------------------------------------------

	public function seo_tags_view()
	{
		$this->load->view('cmoon/include/header',$this->data);
		$data['result'] = $this->cmoon_model->get('seo_tags');
		$this->load->view('cmoon/seo_tags_view',$data);
		$this->load->view('cmoon/include/footer');
	}

		public function seo_tags_add($id = '')
	
	{
		if($this->input->post()){

			$form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);

			if($id != ''){
    //             $del_existing_img = $this->cmoon_model->get_row_by_row('seo_tags', $id);
			 // if ($_FILES['image']['name'] != '') {
    //                 unlink("cmoon_images/".$del_existing_img->image);
    //                 $form_data['image'] = $this->upload_file('image');
    //             }
				$result = $this->cmoon_model->update('seo_tags',$form_data,$id);

			if ($result == true) {
                redirect('cmoon/seo_tags_view/Success');
            } else {
                redirect('cmoon/seo_tags_add/error');
            }
			}else{


              // if ($_FILES['image']['name'] != '') {
              //       $form_data['image'] = $this->upload_file('image');
              //   }
				$result = $this->cmoon_model->insert('seo_tags',$form_data);

			if ($result == true) {
                redirect('cmoon/seo_tags_view/Success');
            } else {
                redirect('cmoon/seo_tags_add/error');
            }
			}

		}else{

			$this->load->view('cmoon/include/header',$this->data);
			if($id != ''){
				$data['result'] = $this->cmoon_model->get_row_by_result('seo_tags',$id);
			}
			$this->load->view('cmoon/seo_tags_add',$data);
			$this->load->view('cmoon/include/footer');
		}
	}


// --------------------------------------    Manage Uploads code    ------------------------------------------

	public function manage_uploads_view()
	{
		$this->load->view('cmoon/include/header',$this->data);		
		$data['result'] = $this->cmoon_model->get('manage_uploads');
		$this->load->view('cmoon/manage_uploads_view',$data);
		$this->load->view('cmoon/include/footer');
	}

	public function manage_uploads_add($id = '')
	{


		if($_FILES['image']['name'] != ''){

			$form_data=$this->input->post();
            // $form_data['p_link'] = $this->permalink($form_data['heading']);

			if($id != ''){
                $del_existing_img = $this->cmoon_model->get_row_by_row('manage_uploads', $id);
			 if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/".$del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
				$result = $this->cmoon_model->update('manage_uploads',$form_data,$id);

			if ($result == true) {
                redirect('cmoon/manage_uploads_view/Success');
            } else {
                redirect('cmoon/manage_uploads_add/error');
            }
			}else{


              if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
                }
				$result = $this->cmoon_model->insert('manage_uploads',$form_data);

			if ($result == true) {
                redirect('cmoon/manage_uploads_view/Success');
            } else {
                redirect('cmoon/manage_uploads_add/error');
            }
			}

		}else{

			$this->load->view('cmoon/include/header',$this->data);
			if($id != ''){
				$data['result'] = $this->cmoon_model->get_row_by_result('manage_uploads',$id);
			}
			$this->load->view('cmoon/manage_uploads_add',$data);
			$this->load->view('cmoon/include/footer');
		}
	}

	   public function manage_uploads_delete($id)
    {


        $del_existing_img = $this->cmoon_model->get_row_by_row('manage_uploads', $id);
            unlink("cmoon_images/".$del_existing_img->image);
        $result = $this->cmoon_model->delete('manage_uploads', $id);
        if ($result == true) {
            redirect('cmoon/manage_uploads_view/dSuccess');
        } else {
            redirect('cmoon/manage_uploads_add/error');
        }
    }



// --------------------------------------     Logs code    ------------------------------------------

    public function logs()
 
	{
		$this->load->view('cmoon/include/header',$this->data);		
		$data['result'] = $this->cmoon_model->get('logs');
		$this->load->view('cmoon/logs',$data);
		$this->load->view('cmoon/include/footer');
	}

// ----------------------------------------------- plink generator  -------------------------------------------------------------

// $post_title = "Welcome to php exampless";
    // echo $permalink = permalink($post_title);
    private function permalink($string)
    {
    	// $string=$string."-".rand(0,9999);
        $string = str_replace(" ", "-", $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

// -------------------------------------- Image upload code -----------------------------------------------------

    private function upload_file($file_name, $path = null)
    {
        $upload_path1             = "cmoon_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "gif|jpg|png|jpeg";
        $config1['max_size']      = "100480000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     = date("YmdHis") . rand(0, 999) . "_" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }


// ----------------------------------- pdf upload code -------------------------------------------
    private function upload_file_brochure($file_name, $path = null)
    {
        $upload_path1             = "cmoon_pdf" . $path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "pdf";
        $config1['max_size']      = "100480000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $img_name1                = preg_replace('-', "_", $img_name1);
        $config1['file_name']     = $img_name1;
        // $config1['file_name']     = date("YmdHis") . rand(0, 999) . "_" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }

    //--------------------- Back up db--------------//
    public function backup_db()
    {
        $site_name=$this->data["site_details"]->site_name;
        $this->load->dbutil();
        $prefs = array(
            'format'   => 'zip',
            'filename' => $site_name,
        );
        $backup = &$this->dbutil->backup($prefs);
        $db_name = $site_name.'-'. date("Y-m-d-H-i-s") . '.zip';
        $save    = 'C:/Users/cmoon/Downloads/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);
        $this->load->helper('download');
        force_download($db_name, $backup);
    }




}