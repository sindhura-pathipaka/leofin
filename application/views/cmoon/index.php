    <section>
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header font-weight-bold "><span class="far fa-window-restore"></span> Site Content</div>
                <div class="card-body">
                    <div class="row">
                        
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/home_banners_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Banners</strong></div>
                            </a>
                        </div>                
                           


                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/cms_pages_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>About & Terms and Conditions</strong></div>
                            </a>
                        </div>                
                           


                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/properties_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Properties</strong></div>
                            </a>
                        </div>                
                           

                              <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/loan_categories_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Loan Categories</strong></div>
                            </a>
                        </div>                
                           

                              <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/real_estate_categories_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Real Estate Categories</strong></div>
                            </a>
                        </div>                
                           

                              <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/insurance_categories_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>Insurance Categories</strong></div>
                            </a>
                        </div>    

                              <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/faqs_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>FAQ's</strong></div>
                            </a>
                        </div>                
                           
                         <div class="col-md-3">

                            <a href="<?php echo base_url(); ?>cmoon/user_accounts" class="anchor">

                                <div class="anchor__icon fas fa-users"></div>

                                <div class="anchor__info"><strong>User Registrations</strong></div>

                            </a>

                        </div>   

                       

                    </div>
                </div>
            </div>
    </div>
     <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header font-weight-bold "><span class="fas fa-cog"></span> SITE SETTINGS</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/site_details" class="anchor">
                                <div class="anchor__icon fas fa-suitcase"></div>
                                <div class="anchor__info"><strong>Site details</strong></div>
                            </a>
                        </div>
                     <!--    <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/social_links" class="anchor">
                                <div class="anchor__icon fas fa-users"></div>
                                <div class="anchor__info"><strong>Social Links & Seo Tags</strong></div>
                            </a>
                        </div>
                         <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/seo_tags_view" class="anchor">
                                <div class="anchor__icon fas fa-align-left"></div>
                                <div class="anchor__info"><strong>SEO Tags</strong></div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/manage_uploads_view" class="anchor">
                                <div class="anchor__icon fas fa-cloud-upload-alt"></div>
                                <div class="anchor__info"><strong>Manage Uploads</strong></div>
                            </a>
                        </div> -->
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/change_password" class="anchor">
                                <div class="anchor__icon fas fa-key"></div>
                                <div class="anchor__info"><strong>Change Password</strong></div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="<?php echo base_url(); ?>cmoon/logs" class="anchor">
                                <div class="anchor__icon fas fa-address-book"></div>
                                <div class="anchor__info"><strong>Logs</strong></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </section>