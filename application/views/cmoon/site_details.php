<section>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="card">

            <form id="site_details" action="<?php echo base_url(); ?>cmoon/site_details" method="POST" enctype="multipart/form-data" autocomplete="off">
               <div style="padding-bottom: 20px;" class="card-header">Site Details <a href="<?php echo base_url(); ?>cmoon"><button style="float: right;" type="button" class="btn btn-outline-dark">Back to menu</button></a></div>

               <div class="card-body">


                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">Site Name</label>
                     <div class="col-md-5"><input type="text" class="form-control" name="site_name" value="<?php echo  $result->site_name; ?>" placeholder="Site Name" /></div>
                  </div>
                    
                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">Site Phone Number</label>
                     <div class="col-md-5"><input type="text" class="form-control" name="site_number"  value="<?php echo  $result->site_number; ?>" placeholder="Phone Number" /></div>
                  </div>

                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">Site Email-Id</label>
                     <div class="col-md-5"><input type="text" class="form-control" name="site_email" value="<?php echo  $result->site_email; ?>"  placeholder="Site Email-Id" /></div>
                  </div>  

                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">From Email-Id</label>
                     <div class="col-md-5"><input type="text" class="form-control" name="from_email" value="<?php echo  $result->from_email; ?>"  placeholder="Site Email-Id" /></div>
                  </div>

                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">Forgot password Email-Id</label>
                     <div class="col-md-5"><input type="text" class="form-control" name="forgot_email" value="<?php echo  $result->forgot_email; ?>"  placeholder="Site Email-Id" /></div>
                  </div>


  
                  </div>



               
               <div class="form-group row">
                  <label class="col-md-2 col-form-label"></label>
                  <div class="col-md-5">
                     <button type="submit" class="btn btn-dark">Submit</button>   
                     <button type="reset" class="btn btn-secondary">Reset</button>
                  </div>
               </div>

                  </div>
                  </form>
             
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

  <script type="text/javascript">
    $(function(){
      // Setup form validation on the #register-form element
        $("#site_details").validate({

        // Specify the validation rules
        rules: {
          site_name: {
              required: true
            },
        site_number: {
              required: true,
               minlength: 10,
               maxlength: 14
            },
          site_email: {
              required: true,
              email: true
            },
             
          from_email: {
              required: true,
              email: true
            },
              
          forgot_email: {
              required: true,
              email: true
            },
            
             phone_number: {
              required: true,
               minlength: 10,
               maxlength: 14
            },
             email_id: {
              required: true,
              email: true
            },
            
               video: {
              required: true
            },
          
                                 
          address: {
              required: true,
              maxlength: 300
            },
          map: {
              required: true
 
            }
        },       
        // Specify the validation error messages
        messages: {
          site_name: {
              required: 'Field should not be empty'
            },
        site_number: {
               required: 'Field should not be empty',
               minlength: 'Please enter a valid mobile number',
               maxlength: 'Please enter a valid mobile number'
            },
          site_email: {
              required: 'Field should not be empty',
              email: 'Please enter a valid email id'
            },
          from_email: {
              required: 'Field should not be empty',
              email: 'Please enter a valid email id'
            },
        forgot_email: {
              required: 'Field should not be empty',
              email: 'Please enter a valid email id'
            },
            
            phone_number: {
               required: 'Field should not be empty',
               minlength: 'Please enter a valid mobile number',
               maxlength: 'Please enter a valid mobile number'
            },
            email_id: {
              required: 'Field should not be empty',
              email: 'Please enter a valid email id'
            },
        video: {
              required: 'Field should not be empty'
            },
        
          address: {
              required: 'Field should not be empty',
              maxlength: 'Only 300 charecters are allowed'
            },
          map: {
              required: 'Field should not be empty'
 

            }
        },
      });
    });
  </script>
  <script type="text/javascript">


function Checkfiles()
{
var fup = document.getElementById('file_type');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == "jpeg"  || ext == "jpg" || ext == "png")
{
return true;
} 
else
{
alert("Upload jpeg,jpg,png Files only");
document.getElementById('file_type').value="";
fup.focus();
return false;
}
}

function Checkfiles1()
{
var fup = document.getElementById('file_type1');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == "jpeg"  || ext == "jpg" || ext == "png")
{
return true;
} 
else
{
alert("Upload jpeg,jpg,png Files only");
document.getElementById('file_type1').value="";
fup.focus();
return false;
}
}

function Checkfiles2()
{
var fup = document.getElementById('file_type2');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == "jpeg"  || ext == "jpg" || ext == "png")
{
return true;
} 
else
{
alert("Upload jpeg,jpg,png Files only");
document.getElementById('file_type2').value="";
fup.focus();
return false;
}
}


  </script>

