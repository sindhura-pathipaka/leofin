<section>

    <div class="container-fluid">   
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="card">


    <?php if($this->uri->segment(3) != '') { 
               foreach ($result as $row);  } ?>


               <div class="card-header"> 
               <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon/seo_tags_view"><button style="float: right;" class="btn btn-dark" type="button"><span class="far fa-arrow-alt-circle-left "> </span>  Back </button></a>
               Edit Seo Tag's</div>
             </div>
               <div class="card-body">
                     <form id="edit_details" onsubmit="return validate()" method="POST" enctype="multipart/form-data" >

                 <div class="form-group row">
                     <label class="col-md-2 col-form-label">Title</label>
                     <div class="col-md-5"><input type="text" class="form-control" id="heading" name="heading" value="<?php echo $row->heading; ?>" placeholder="title" />
                     </div>
                  </div>
                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">Meta Title</label>
                     <div class="col-md-5">
                      <textarea class="form-control" rows="5" name="meta_title" placeholder="Meta Title"><?php echo $row->meta_title; ?></textarea></div>
                  </div>

                  <div class="form-group row">
                     <label class="col-md-2 col-form-label">Meta keywords</label>
                     <div class="col-md-5">
                      <textarea class="form-control" rows="5" name="meta_keywords" placeholder="Meta keywords"><?php echo $row->meta_keywords; ?></textarea></div>
                  </div>

                 <div class="form-group row">
                     <label class="col-md-2 col-form-label">Meta Description</label>
                     <div class="col-md-5">
                      <textarea class="form-control" rows="5" name="meta_description" placeholder="Meta Description"><?php echo $row->meta_description; ?></textarea></div>
                  </div>


                <div class="col-md-9 offset-md-3">
                         <button type="submit" class="btn btn-primary">Submit</button>
                         <button type="reset" class="btn btn-secondary">Reset</button>
                                  <!--  <button type="reset" class="btn btn-primary"><span class="far fa-undo "> </span></button> -->
                              </div>

</form>
               </div>            
                     </div>
                  </form>
                </div>
            </div>

         </div>
      </div>
   </div>
</section>