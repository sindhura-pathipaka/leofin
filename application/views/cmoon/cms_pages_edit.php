<section>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="card">

    <?php if($this->uri->segment(3) != '') { 

               foreach ($result as $row);  } ?>
    
            <form id="cms_pages"  method="POST" enctype="multipart/form-data" autocomplete="off">
               <div style="padding-bottom: 20px;" class="card-header">Add or Edit CMS Pages <a href="<?php if($row->id == 44){ ?>cms_pages_view <?php }elseif($row->id == 1){ ?> cms_pages_view <?php }elseif($row->id == 2){ ?> cms_pages_view <?php } ?>"><button style="float: right;" type="button" class="btn btn-outline-dark">Back</button></a></div>
               <div class="card-body">


        <div class="form-group row">
           <label class="col-md-2 col-form-label">Heading</label>
           <div class="col-md-5"><input type="text" class="form-control" name="heading" value="<?php echo $row->heading; ?>"  placeholder="Heading" /></div>
         </div>

 

<?php if($row->id == 1){ ?>

<input type="hidden" name="redirect" value="cms_pages_view">
<?php }elseif($row->id == 2){ ?> 

<input type="hidden" name="redirect" value="cms_pages_view">
 <?php }    ?>

<?php if($row->id != 1 && $row->id != 2){ ?>


 <?php if($row->image != '' ){ ?>

                    <div class="form-group row">
                     <label class="col-md-2 col-form-label"></label>
                    <div class="col-md-5"><a data-fancybox="gallery" href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>"><img style="width: 300px" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>" class="img-fluid"></a></div>
                  </div>
    <?php } ?>
              
      <div class="form-group row">
         <label class="col-md-2 col-form-label">Image</label>
          <div class="col-md-5"><input type="file" class="form-control" name="image" id="file_type" onchange="Checkfiles()"/>
          <i><b>Note :</b> Only Jpeg, jpg, png format images are allowed Please upload Below 2MB images  </i>

        </div>
      </div>
<?php }    ?>


        <div class="form-group row">
           <label class="col-md-2 col-form-label">Context</label>
           <div class="col-md-8">

            <textarea class="ckeditor" rows="5" name="description"  placeholder="Description"><?php echo $row->description; ?></textarea>
          </div>
         </div>

     

         <div class="form-group row">
                      <label class="col-md-2 col-form-label"></label>
                      <div class="col-md-5">
                         <button type="submit" class="btn btn-primary">Submit</button>
                         <button type="reset" class="btn btn-secondary">Reset</button>
                      </div>
                   </div>
    
               </div>
                  
               </form>
             
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

  <script type="text/javascript">
    $(function(){
      // Setup form validation on the #register-form element
        $("#cms_pages").validate({

        // Specify the validation rules
        rules: {
          heading: {
              required: true
            },
           
            sub_heading: {
              required: true
            },

           
            description: {
              required: true
            },
              description1: {
              required: true
            }
        },       
        // Specify the validation error messages
        messages: {
          heading: {
              required: 'Field should not be empty'
            },
          
         sub_heading: {
              required: 'Field should not be empty'
            },

             description: {
              required: 'Field should not be empty'
            },
              description1: {
              required: 'Field should not be empty'
            }
          }
      });
    });





    function Checkfiles()
{
var fup = document.getElementById('file_type');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == "jpeg"  || ext == "jpg" || ext == "png")
{
return true;
} 
else
{
alert("Upload jpeg,jpg,png Files only");
document.getElementById('file_type').value="";
fup.focus();
return false;
}
}


    function Checkfiles1()
{
var fup = document.getElementById('pdf');
var fileName = fup.value;
var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
if(ext == 'pdf' || ext== 'doc' || ext == 'docx' )
{
return true;
} 
else
{
alert("Upload pdf Files only");
document.getElementById('pdf').value="";
fup.focus();
return false;
}
}


</script>