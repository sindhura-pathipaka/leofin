    <section>
        <div class="container-fluid">
                  <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col">User Accounts</div>
                        <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon"><button style="float: right;" class="btn btn-outline-dark" type="button"><span class="far fa-arrow-alt-circle-left "> </span>  Back to Menu</button></a></div>
                       <!--  <div style="padding-bottom: 20px;"><a href="<?php echo base_url(); ?>cmoon/careers_add"><button style="float: right;" class="btn btn-outline-dark" type="button">Add careers</button></a></div> -->
              </div>
          </div>
<div class="card-body">
<div class="row mb-3">
</div>    
<div class="table-responsive">
   <table class="example table table-striped table-bordered nowrap" style="width:100%">
    <thead>
      <tr>
        <th>Sl.No</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Status</th>
        <th>Details</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1;  foreach ($result as $row) {   ?>
            <tr>
                <td> <?php echo $no; ?> </td>
                <td> <?php echo $row->name; ?> </td>
                <td> <?php echo $row->email; ?> </td>
                <td> <?php echo $row->mobile; ?> </td>
                <td> <?php echo $row->account_status; ?> </td>
                <td> 
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo $row->id; ?>">  Full Details </button>
                </td>
                <td>
                   <a href="<?php echo base_url(); ?>cmoon/user_accounts_edit/<?php echo $row->id; ?>" class="btn btn-outline-success"> Edit Status</a> &nbsp; &nbsp; 
                <a onclick="ConfirmDelete(<?php echo $row->id; ?>)" href="JavaScript:Void(0);" class="btn btn-outline-danger">Delete</a> </td>
            </tr>
          <?php $no++; } ?>
    </tbody>
</table>
</div></div></div></div>
    </section>
      <?php  foreach ($result as $row) {   ?>
               <!-- Modal -->
<div class="modal fade" data-backdrop="false" id="exampleModal<?php echo $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Full Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
             <u>Name</u> :  <?php echo $row->first_name; ?> </br></br> 
             <u>Email</u> :       <?php echo $row->email; ?> </br></br> 
             <u>Phone Number</u> : <?php echo $row->mobile; ?> </br></br> 
             <!-- <u>Reg No</u> :      <?php echo $row->reg_no; ?></br></br> -->
             <u>Status</u> :      <?php echo $row->account_status; ?></br></br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<script type="text/javascript">
    $('.example').DataTable({
  responsive: true
});
</script>
<script type="text/javascript">
      function ConfirmDelete(id)
      {
        swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
window.location.href='user_accounts_delete/'+id;       
}
});
}
</script>