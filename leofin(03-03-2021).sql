-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2021 at 02:16 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `leofin`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_ips`
--

CREATE TABLE `admin_ips` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_ips`
--

INSERT INTO `admin_ips` (`id`, `ip_address`) VALUES
(1, '::1'),
(2, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL,
  `heading` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `p_link` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `redirect` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `heading`, `image`, `description`, `p_link`, `redirect`) VALUES
(1, 'About', NULL, '<p>Description</p>\r\n', 'About', 'cms_pages_view'),
(2, 'terms and conditions', NULL, '<p>description</p>\r\n', 'terms-and-conditions', 'cms_pages_view');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `heading` text DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `heading`, `description`) VALUES
(1, 'question', 'answer');

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(11) NOT NULL,
  `image` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_banners`
--

INSERT INTO `home_banners` (`id`, `image`) VALUES
(1, '2021030212184691_service_11.jpg'),
(2, '20210302165043390_service_11.jpg'),
(3, '20210302170608622_commercial1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_categories`
--

CREATE TABLE `insurance_categories` (
  `id` int(11) NOT NULL,
  `heading` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `insurance_categories`
--

INSERT INTO `insurance_categories` (`id`, `heading`) VALUES
(1, 'Health Insurance');

-- --------------------------------------------------------

--
-- Table structure for table `loan_categories`
--

CREATE TABLE `loan_categories` (
  `id` int(11) NOT NULL,
  `heading` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loan_categories`
--

INSERT INTO `loan_categories` (`id`, `heading`) VALUES
(1, 'Car Loan');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'admin', '34b329d9495816e93f9ff420d891b218676e8b60');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `ip_address` varchar(250) NOT NULL,
  `login_time` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `manage_uploads`
--

CREATE TABLE `manage_uploads` (
  `id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_uploads`
--

INSERT INTO `manage_uploads` (`id`, `image`) VALUES
(2, '20191213124035582_slide01.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `heading` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `category` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `location` text DEFAULT NULL,
  `posted_on` text DEFAULT NULL,
  `str_time` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `heading`, `image`, `category`, `description`, `location`, `posted_on`, `str_time`) VALUES
(1, 'Property title', '20210302165043390_service_1.jpg', 'Buy', '<p>description</p>\r\n', 'address', '02-Mar-2021', '1614623400'),
(2, 'Property Title', '20210302170608622_commercial.jpg', 'Sell', '<p>description 1</p>\r\n', 'address comes here', '02-Mar-2021', '1614623400');

-- --------------------------------------------------------

--
-- Table structure for table `real_estate_categories`
--

CREATE TABLE `real_estate_categories` (
  `id` int(11) NOT NULL,
  `heading` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `real_estate_categories`
--

INSERT INTO `real_estate_categories` (`id`, `heading`) VALUES
(1, 'buy');

-- --------------------------------------------------------

--
-- Table structure for table `seo_tags`
--

CREATE TABLE `seo_tags` (
  `id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seo_tags`
--

INSERT INTO `seo_tags` (`id`, `heading`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 'About Us', 'GLP Pharma Standards', '', ''),
(2, 'Services', '', '', ''),
(3, 'Products', '', '', ''),
(4, 'Medicines', '', '', ''),
(5, 'Gallery', '', '', ''),
(6, 'Contact Us', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `site_details`
--

CREATE TABLE `site_details` (
  `id` int(11) NOT NULL,
  `site_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `site_number` varchar(250) NOT NULL,
  `site_email` varchar(250) NOT NULL,
  `from_email` varchar(250) NOT NULL,
  `forgot_email` varchar(250) NOT NULL,
  `video` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_details`
--

INSERT INTO `site_details` (`id`, `site_name`, `site_number`, `site_email`, `from_email`, `forgot_email`, `video`) VALUES
(1, 'Leofin', '1234567890', 'sindhura.colourmoon@gmail.com', 'Info@thecolourmoon.Com', 'Info@thecolourmoon.Com', 'wj8HDzeoJbQ');

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE `social_links` (
  `id` int(11) NOT NULL,
  `facebook` varchar(250) NOT NULL,
  `twitter` varchar(250) NOT NULL,
  `youtube` varchar(250) NOT NULL,
  `skype` varchar(250) NOT NULL,
  `linkedin` varchar(250) NOT NULL,
  `google` varchar(255) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_keywords` varchar(1500) NOT NULL,
  `meta_description` varchar(1500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_links`
--

INSERT INTO `social_links` (`id`, `facebook`, `twitter`, `youtube`, `skype`, `linkedin`, `google`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 'https://www.facebook.com/thecolormoon/', 'https://twitter.com/login', 'https://www.youtube.com/?gl=IN', 'https://www.skype.com/en/', 'https://www.linkedin.com/uas/login?', 'https://plus.google.com/', 'GLP Pharma Standards', 'GLP Pharma Standards', 'GLP Pharma Standards');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `name` text DEFAULT NULL,
  `mobile` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `created_at` text DEFAULT NULL,
  `reg_no` text DEFAULT NULL,
  `otp` text DEFAULT NULL,
  `account_status` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `device_id`, `name`, `mobile`, `email`, `password`, `created_at`, `reg_no`, `otp`, `account_status`) VALUES
(1, '123', 'test', '1212121212', 'test@gmail.com', 'ceb6c970658f31504a901b89dcd3e461', '03-03-2021 04:12:01 PM', '714058', '7480', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` int(11) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `ip_address` varchar(250) NOT NULL,
  `login_time` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_logs`
--

INSERT INTO `user_logs` (`id`, `mobile`, `ip_address`, `login_time`) VALUES
(1, '1212121212', '::1', '03-03-2021 04:52:05 PM'),
(2, '1212121212', '::1', '03-03-2021 06:00:48 PM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_ips`
--
ALTER TABLE `admin_ips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_categories`
--
ALTER TABLE `insurance_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_categories`
--
ALTER TABLE `loan_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_uploads`
--
ALTER TABLE `manage_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `real_estate_categories`
--
ALTER TABLE `real_estate_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_tags`
--
ALTER TABLE `seo_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_details`
--
ALTER TABLE `site_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_links`
--
ALTER TABLE `social_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_ips`
--
ALTER TABLE `admin_ips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `insurance_categories`
--
ALTER TABLE `insurance_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `loan_categories`
--
ALTER TABLE `loan_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `real_estate_categories`
--
ALTER TABLE `real_estate_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
